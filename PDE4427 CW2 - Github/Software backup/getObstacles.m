function DN = getObstacles()

laserscan = rossubscriber('/scan');

pause(1);

% 0 to 359 degrees (in radians)
angles = [0:pi/180:(2*pi - (pi/180))];

ang1 = [angles(337:end) angles(1:22)];
ang2 = [angles(23:67)];
ang3 = [angles(68:112)];
ang4 = [angles(113:157)];
ang5 = [angles(158:202)];
ang6 = [angles(203:247)];
ang7 = [angles(248:292)];
ang8 = [angles(293:336)];

AllDists = [];
AllAngSpans = [];
AllWdths = [];
    
% 360 values
rangevals = laserscan.LatestMessage.Ranges;
rangevals = rangevals';

% Sector 1
p = [rangevals(337:end) rangevals(1:22)];
[distn,angspan,wdth] = getNearestObjDetails(p,ang1);
AllDists = [distn];
AllAngSpans = [angspan(1) angspan(end)];
AllWdths = [wdth];

% Sector 2
p = [rangevals(23:67)];
[distn,angspan,wdth] = getNearestObjDetails(p,ang2);
AllDists = [AllDists ; distn];
AllAngSpans = [AllAngSpans ; angspan(1) angspan(end)];
AllWdths = [AllWdths ; wdth];

% Sector 3
p = [rangevals(68:112)];
[distn,angspan,wdth] = getNearestObjDetails(p,ang3);
AllDists = [AllDists ; distn];
AllAngSpans = [AllAngSpans ; angspan(1) angspan(end)];
AllWdths = [AllWdths ; wdth];

% Sector 4
p = [rangevals(113:157)];
[distn,angspan,wdth] = getNearestObjDetails(p,ang4);
AllDists = [AllDists ; distn];
AllAngSpans = [AllAngSpans ; angspan(1) angspan(end)];
AllWdths = [AllWdths ; wdth];

% Sector 5
p = [rangevals(158:202)];
[distn,angspan,wdth] = getNearestObjDetails(p,ang5);
AllDists = [AllDists ; distn];
AllAngSpans = [AllAngSpans ; angspan(1) angspan(end)];
AllWdths = [AllWdths ; wdth];

% Sector 6
p = [rangevals(203:247)];
[distn,angspan,wdth] = getNearestObjDetails(p,ang6);
AllDists = [AllDists ; distn];
AllAngSpans = [AllAngSpans ; angspan(1) angspan(end)];
AllWdths = [AllWdths ; wdth];

% Sector 7
p = [rangevals(248:292)];
[distn,angspan,wdth] = getNearestObjDetails(p,ang7);
AllDists = [AllDists ; distn];
AllAngSpans = [AllAngSpans ; angspan(1) angspan(end)];
AllWdths = [AllWdths ; wdth];

% Sector 8
p = [rangevals(293:336)];
[distn,angspan,wdth] = getNearestObjDetails(p,ang8);
AllDists = [AllDists ; distn];
AllAngSpans = [AllAngSpans ; angspan(1) angspan(end)];
AllWdths = [AllWdths ; wdth];

DN = AllDists;

end