% ROS setup

rosshutdown;
rosinit('192.168.1.110');

setenv('ROS_MASTER_URI','http://192.168.1.110:11311')
setenv('ROS_IP','192.168.1.115')



% make this a rospublisher for /move_position
posPub = rospublisher('/move_position');
posMsg = rosmessage(posPub);

% enter the INITAL values for the goal angle and distance here
goalAngle = 0.0;
goalDistance = 1.2; % this is in metres
GRIDSIZE = 0.2; %in metres (converted to cm later)
MOVETIME = 3; %time to wait for turtle to move (in seconds)
TOLERANCE = 0.08;

while goalDistance > TOLERANCE

    AN = getAngles(goalAngle, goalDistance, GRIDSIZE, TOLERANCE);
    DN = getObstacles();
    
    AN
    DN

    Rank = [];
    fis = readfis('fuzzyGroup.fis');

    for i=1:8
        newval = evalfis(fis, [AN(i) DN(i)]);
        Rank = [Rank newval];
    end

    [rank nextPos] = max(Rank);
    
    % publish the new position and then wait for robot to move
    posMsg = rosmessage(posPub);
    posMsg.Data = nextPos;
    send(posPub, posMsg);
    pause(2);
    posMsg.Data = 9;
    send(posPub, posMsg);
    
    % while the robot is moving we can update the goal info
    [goalAngle, goalDistance] = goalie(goalAngle, goalDistance*100, nextPos, GRIDSIZE*100);
    goalDistance = goalDistance / 100; 
    
    pause(MOVETIME);
end