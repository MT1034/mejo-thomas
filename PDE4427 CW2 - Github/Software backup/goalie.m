function [newGoalAngle, newGoalDistance] = goalie(goalAngle, goalDistance, nn, GRIDSIZE)
%calculates the new goal (angle and distance) given the current goal and
%the nearest neighbour
%   GRIDSIZE is caps because it is a constant that is passed in so this
%   function can be reused (potentially) - although it's needed here in cm
    
angle = 0.0;
distance = goalDistance;
side = GRIDSIZE;
turn = goalAngle; %this is to determine if newGoalAngle is +ve or -ve

if (nn == 1)
    angle = goalAngle;
elseif (nn == 2)
    angle = -1*((pi/4)-goalAngle);
    side = sqrt(GRIDSIZE*GRIDSIZE*2); 
    turn = goalAngle - pi/4;
elseif (nn == 3)
    angle = -1*((pi/2)-goalAngle);
    turn = goalAngle - pi/2;
elseif (nn == 4)
    angle = -1*((3*pi/4)-goalAngle);
    side = sqrt(GRIDSIZE*GRIDSIZE*2); 
    turn = goalAngle - (3*pi/4);
elseif (nn == 5)
    angle = -1*(pi-goalAngle);
    turn = goalAngle - pi;
elseif (nn == 6)
    angle = goalAngle+(3*pi/4);
    side = sqrt(GRIDSIZE*GRIDSIZE*2);
    turn = pi/4 - goalAngle;
elseif (nn == 7)
    angle = goalAngle+(pi/2);
    turn = pi/2 - goalAngle;
elseif (nn == 8)
    angle = goalAngle+(pi/4);
    side = sqrt(GRIDSIZE*GRIDSIZE*2);
    turn = (3*pi/4) - goalAngle;
end

newGoalDistance = sqrt(distance*distance + side*side - 2*distance*side*cos(angle));
%sine rule has a problem with angles over 90
%newGoalAngle = asin(distance*sin(angle)/newGoalDistance);
newGoalAngle = pi-acos(((newGoalDistance*newGoalDistance)+(side*side)-(distance*distance))/(2*side*newGoalDistance));
if (turn < 0)
    newGoalAngle = newGoalAngle * -1;
end
end

