function [distn,angspan,wdth] = getNearestObjDetails(pp,angg)

maxdist = 2;
tol = 0.1;
start = 1;
stop = 1;
maxelts = 46;
v = zeros(1,maxelts);
w = zeros(1,maxelts);
p = [];
ang = [];


% Find nonzero elements in distances vector and remove all zeros
[row,col,p] = find(pp);
ang = angg(col);

% Remove any noise from the data
if length(col) > 1 && col(2) ~= col(1) + 1
    D = diff([0,diff(col)==1,0]);
    first = col(D>0);
    last = col(D<0);
    
    if isempty(first)
        p = [];
        ang = [];
    else
        p = [];
        ang = [];
        
        for k = 1:length(first)
            p = [p pp(first(k):last(k))];
            ang = [ang angg(first(k):last(k))];
        end
    end
end

% Create bins corresponding to each object viewed
for i = 1:length(p)-1
    
    if abs(p(i) - p(i+1)) > tol
        stop = i;
        if start ~= stop
            v = [v ; p(start:stop) zeros(1,(maxelts - (stop-start) - 1))];
            w = [w ; ang(start:stop) zeros(1,(maxelts - (stop-start) - 1))];
        else
            v = [v ; p(start) zeros(1,maxelts-1)];
            w = [w ; ang(start) zeros(1,maxelts-1)];
        end
        start = i+1;
    end
    
    if i == length(p) - 1
        stop = i + 1;
        if start ~= stop
            v = [v ; p(start:stop) zeros(1,(maxelts - (stop-start) - 1))];
            w = [w ; ang(start:stop) zeros(1,(maxelts - (stop-start) - 1))];
        else
            v = [v ; p(start) zeros(1,maxelts-1)];
            w = [w ; ang(start) zeros(1,maxelts-1)];
        end
    end
    
end


if length(p) == 1 || isempty(p)
    
    % Special Case - Either p is empty or has only one element
    distn = maxdist;
    angspan = [angg(1) angg(end)];
    wdth = sin(abs(angspan(end) - angspan(1)))*distn;
    
else
    
    % the main case 
    
    % Removing 1st rows - all zeros - since v and w were initialized to row
    % of zeros (that was a dummy row)
    v = v(2:end,:);
    w = w(2:end,:);
    [distn ind ] = min(v(v > 0));
    
    % Find Exact location in matrix
    [indr, indc] = ind2sub(size(v),ind);
    
    flag = 0;
    while flag == 0
        if v(indr,2) == 0
            v(indr,:) = [];
            w(indr,:) = [];
        else
            flag = 1;
            break;
        end
        
        [distn ind ] = min(v(v > 0));
        [indr, indc] = ind2sub(size(v),ind);
    end
    
    
    % get angle span for each object
    x = w(indr,:);
    angspan = x(x~=0);
    
    % get width of each object
    wdth = abs(sin(abs(angspan(end) - angspan(1))))*distn;
    
    
end

% distance normalization
if distn > 2
    distn = maxdist;
end
distn = distn / 2;

end