#include <ArduinoHardware.h>
#include <ros.h>
#include <std_msgs/Float32.h>


ros::NodeHandle nh;

std_msgs::Float32 float32_msg;

ros::Publisher pub("light", &float32_msg);

#define pwmA 5
#define pwmB 11
#define in1 6
#define in2 7
#define in3 8
#define in4 9
#define LS  4    // left sensor
#define RS  3    // right sensor

const int ldrPin = A2;
int dir = 0;
int pwmOutput = 255;


void setup() 

{
pinMode(pwmA,OUTPUT);
pinMode(pwmB,OUTPUT);
pinMode(in1,OUTPUT);
pinMode(in2,OUTPUT);
pinMode(in3,OUTPUT);
pinMode(in4,OUTPUT);
pinMode(RS,INPUT);
pinMode(LS,INPUT);



Serial.begin(57600);
nh.initNode();
nh.advertise(pub);

}


void loop() 

{

analogWrite(pwmA, pwmOutput);
analogWrite(pwmB, pwmOutput);


int ldrStatus = analogRead(ldrPin);
float lightdisplay = ldrStatus ;
float32_msg.data = lightdisplay ;
pub.publish( &float32_msg);

Serial.println(lightdisplay);

nh.spinOnce();

    
Serial.print(digitalRead(RS));
Serial.println(digitalRead(LS));


if((digitalRead(LS)==LOW) && (digitalRead(RS)==LOW))    // Condition_1 forward

 {
  forward();
  Serial.println("FORWARD");analogWrite(pwmA, pwmOutput);
analogWrite(pwmB, pwmOutput);
 }
 
else if((digitalRead(LS)==HIGH) && (digitalRead(RS)==HIGH))  //CONDITION-2 Forward (Both IR ON)

 {
  if(dir==0){
  forward();
  }
 if(dir==1){
 forward();
 }
 Serial.println("Forward (Both IR ON)");
 }
else if((digitalRead(LS)==LOW) && (digitalRead(RS)==HIGH))  // LEFT 
  { 
    dir = 0;
    TurnLeft();
    Serial.println("left");
  }
 else if((digitalRead(LS)==HIGH) && (digitalRead(RS)==LOW))  // RIGHT
  {
    dir = 1;
    TurnRight();
    Serial.println("Right");
  }
}

void forward() 

{
analogWrite(pwmA, pwmOutput);
analogWrite(pwmB, pwmOutput);
digitalWrite(in1, HIGH);
digitalWrite(in2, LOW);
digitalWrite(in3, HIGH);
digitalWrite(in4, LOW);
} 

void TurnLeft() {
analogWrite(pwmA, pwmOutput);
analogWrite(pwmB, pwmOutput);
digitalWrite(in1, LOW);
digitalWrite(in2, HIGH);
digitalWrite(in3, HIGH);
digitalWrite(in4, LOW);
} 

void TurnRight() {
analogWrite(pwmA, pwmOutput);
analogWrite(pwmB, pwmOutput);
digitalWrite(in1, HIGH);
digitalWrite(in2, LOW);
digitalWrite(in3, LOW);
digitalWrite(in4, HIGH);
} 

void StopMotors() {
digitalWrite(in1, LOW);
digitalWrite(in2, LOW);
digitalWrite(in3, LOW);
digitalWrite(in4, LOW);
}
