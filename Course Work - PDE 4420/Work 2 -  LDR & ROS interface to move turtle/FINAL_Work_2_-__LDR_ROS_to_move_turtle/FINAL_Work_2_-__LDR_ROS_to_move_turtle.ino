// Sample LED ON / OFF program was referred from web resources
// ROS interface was referred from standard tutorial exercises of ROS wiki 
// Support was also obtained from Bittu, colleague on a few areas 



#include <ArduinoHardware.h>
#include <ros.h>
#include <geometry_msgs/Twist.h>

ros::NodeHandle nh;

geometry_msgs::Twist msg;

ros::Publisher pub("turtle1/cmd_vel", &msg);

const int ledPin = 6;

const int ledPin1 = 7;


const int ldrPin = A0;

void setup() {

Serial.begin(57600);

pinMode(ledPin, OUTPUT);

pinMode(ldrPin, INPUT);

nh.initNode();
nh.advertise(pub);

}

void loop() {

int ldrStatus = analogRead(ldrPin);

Serial.println(ldrStatus);
if (ldrStatus <=15) {

digitalWrite(ledPin, HIGH);

        if(digitalRead(6)==1)
        msg.linear.x=-0.25;
        digitalWrite(ledPin1, LOW);
        
delay(200);
}

else if (ldrStatus >15) {

digitalWrite(ledPin1, HIGH);
       if(digitalRead(7)==1)
       digitalWrite(ledPin, LOW);
        msg.linear.x= 0.25;

}

else if (digitalRead(6)==0 && digitalRead(7)==0)
msg.linear.x=0;

pub.publish(&msg);
nh.spinOnce();



}
